# Description

This project contains the files served by [mjpg-streamer](https://github.com/jacksonliam/mjpg-streamer) when R1D3 stream its webcam. 

The streaming is launched with the following command on the robot:

```
./mjpg_streamer -i "./input_uvc.so" -o "./output_http.so -p 8090 -w ../www_agayon"
```

![Capture](capture_stream.png)

