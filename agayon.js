function callApi(action) {
    const baseUrl = window.location.origin;
    const xhr = new XMLHttpRequest();
    xhr.open('POST', baseUrl + '/api/actions/' + action, true);
    let data = new FormData();
    data.append('direction', action);
    xhr.onload = function () {
        console.log("Action " + action + ' Success');
    };
    xhr.send(data);
}

function up(){
    callApi('up');
}
function down(){
    callApi('down');
}
function left(){
    callApi('left');
}
function right(){
    callApi('right');
}
function smallLeft(){
    callApi('smallLeft');
}
function smallRight(){
    callApi('smallRight');
}
function stopRobot(){
    callApi('stopRobot');
}
function stopStream(){
	setTimeout(function(){ 
        location.reload();
    }, 8000);
    callApi('stopStream');
}
function record(){
    callApi('RPIrecord');
}

function lidarShot(){
	setTimeout(function(){ 
        location.reload();
    }, 5000);
    callApi('lidarMapping');
}
